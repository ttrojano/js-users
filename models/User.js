class User {
    
    constructor(name, email, password, admin){

        this._name = name;
        this._email = email;
        this._password = password;
        this._admin = admin;

    }

    get name(){
        return this._name;
    }

    get email() {
        return this._email;
    }

    get password() {
        return this._password;
    }

    get admin() {
        return this._admin;
    }

    

    static getUsersStorage() {

        let users = [];

        if (localStorage.getItem("users")) {
            users = JSON.parse(localStorage.getItem("users"));
        }

        return users;

    }

    remove(){

        let users = User.getUsersStorage();
        users.forEach((userData, index)=>{

            if (this._id == userData._id) {
                users.splice(index, 1);
            }

        });

        localStorage.setItem("users", JSON.stringify(users));

    }


}