class UserController {

    constructor(formIdCreate, tableId){
        this.formEl = document.getElementById(formIdCreate);
        this.tableEl = document.getElementById(tableId);
    }

    onSubmit(){
        this.formEl.addEventListener("submit", event => {
             event.preventDefault();
             let values = this.getValues(this.formEl)
             console.log(values)
             if (!values) {
                 return false;

             }
             this.addLine(values);
             this.formEl.reset();
             
             
        });
    }

    getValues(formEl){
        
        let user = {};
        let isValid = true;

        [...formEl.elements].forEach(function(field){

            if (['name', 'email', 'password'].indexOf(field.name) > -1 && !field.value) {
                field.parentElement.classList.add('has-error');
                isValid = false;
            }

            if (field.name == "admin"){
                user[field.name] = field.checked;    
            } else {
                user[field.name] = field.value;
            }
            
        });
        
        if (!isValid) {
            return false;
        }

        return new User(
            user.name, 
            user.email, 
            user.password, 
            user.admin);
    }

    addLine(dataUser) {
        
        let tr = this.getTr(dataUser);
        this.tableEl.appendChild(tr);
        this.updateCount();
    }

    getTr(dataUser, tr = null){

        if (tr === null) tr = document.createElement('tr');

        tr.dataset.user = JSON.stringify(dataUser);

        tr.innerHTML = `
            <td>${dataUser.name}</td>
            <td>${dataUser.email}</td>
            <td>${(dataUser.admin) ? 'Yes' : 'No'}</td>
            
            <td>
                <button type="button" class="btn btn-danger btn-delete btn-xs btn-flat">Excluir</button>
            </td>
        `;

        this.addEventsTr(tr);
        return tr;

    }
    
    addEventsTr(tr){

        console.log("Aqui");    
        tr.querySelector(".btn-delete").addEventListener("click", e => {

            if (confirm("Are you sure you want to delete this?")) {
                let user = new User();
                user.remove();
                tr.remove();
                this.updateCount();
            }
        });
    }

    updateCount(){

        let numberUsers = 0;
        let numberAdmin = 0;

        [...this.tableEl.children].forEach(tr=>{

            numberUsers++;
            
            let user = JSON.parse(tr.dataset.user);

            if (user._admin) numberAdmin++;
            
        });

        document.querySelector("#number-users").innerHTML = numberUsers;
        document.querySelector("#number-users-admin").innerHTML = numberAdmin;

    }




}